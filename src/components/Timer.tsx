import React, { FC, useEffect, useRef, useState } from 'react';
import { Player } from "../models/Player.ts";
import { Colors } from "../models/Colors.ts";

interface TimerProps {
  currentPlayer: Player | null;
  restart: () => void;
  blackTime: number;
  whiteTime: number;
  setBlackTime: () => void;
  setWhiteTime: () => void;
}
const Timer: FC<TimerProps> = ({ currentPlayer, restart, whiteTime, blackTime, setBlackTime, setWhiteTime }) => {
  const timer = useRef<null | ReturnType<typeof setInterval>>(null)

  useEffect(() => {
    startTimer()
  }, [currentPlayer])

  function startTimer() {
    if (timer.current) {
      clearInterval(timer.current)
    }
    const callback = currentPlayer?.color === Colors.WHITE ? decrementWhiteTimer : decrementBlackTimer
    timer.current = setInterval(callback, 1000)
  }


  function decrementBlackTimer() {
    setBlackTime(prev => prev - 1)
  }
  function decrementWhiteTimer() {
    setWhiteTime(prev => prev - 1)
  }

  const handleRestart = () => {
    setWhiteTime(300)
    setBlackTime(300)
    restart()
  }

  return (
    <div>
      <div>
        <button onClick={handleRestart}>Restart game</button>
      </div>
      <h2> Черне - {blackTime}</h2>
      <h2> Белые - {whiteTime}</h2>
    </div>
  );
};

export default Timer;
