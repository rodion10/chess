import React, { FC } from 'react';
import { Figure } from "../models/figures/Figure.ts";

interface LostFiguresProps {
  title: string;
  figures: Figure[]
  restart: () => void;
}
const LostFigures: FC<LostFiguresProps> = ({ title, figures, restart }) => {
  let row = figures.map(function (figure, i) {
    if (figure.name === "Король") {
      return <div className='gameover'>
        <div className='gameover2'>
          <h2>Gameover</h2>
          <button onClick={restart}>Restart game</button>
        </div>
      </div>
    }
    return <div key={figure.id}>
      {figure.name} {figure.logo && <img width={20} height={20} src={figure.logo} />}
      {console.log(figure.name)}
    </div>
  }
  )
  return (
    <div className="lost">
      <h3>{title}</h3>
      {row}
    </div>
  );
};
export default LostFigures;